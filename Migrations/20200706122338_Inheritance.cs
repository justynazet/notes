﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notes.Migrations
{
    public partial class Inheritance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Image_Note_NoteId",
                table: "Image");

            migrationBuilder.DropForeignKey(
                name: "FK_Item_List_ListId",
                table: "Item");

            migrationBuilder.DropTable(
                name: "List");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Note",
                table: "Note");

            migrationBuilder.RenameTable(
                name: "Note",
                newName: "Base");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Base",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Base",
                table: "Base",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Base_NoteId",
                table: "Image",
                column: "NoteId",
                principalTable: "Base",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Item_Base_ListId",
                table: "Item",
                column: "ListId",
                principalTable: "Base",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Image_Base_NoteId",
                table: "Image");

            migrationBuilder.DropForeignKey(
                name: "FK_Item_Base_ListId",
                table: "Item");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Base",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Base");

            migrationBuilder.RenameTable(
                name: "Base",
                newName: "Note");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Note",
                table: "Note",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "List",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Color = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Note_NoteId",
                table: "Image",
                column: "NoteId",
                principalTable: "Note",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Item_List_ListId",
                table: "Item",
                column: "ListId",
                principalTable: "List",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
