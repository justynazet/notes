﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notes.Migrations
{
    public partial class IdentityModelBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base_AspNetUsers_Note_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropIndex(
                name: "IX_Base_Note_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "Note_IdentityUserId",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "Note_IdentityUserId1",
                table: "Base");

            migrationBuilder.AlterColumn<int>(
                name: "IdentityUserId",
                table: "Base",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdentityUserId",
                table: "Base",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Note_IdentityUserId",
                table: "Base",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note_IdentityUserId1",
                table: "Base",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Base_Note_IdentityUserId1",
                table: "Base",
                column: "Note_IdentityUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Base_AspNetUsers_Note_IdentityUserId1",
                table: "Base",
                column: "Note_IdentityUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
