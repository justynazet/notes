﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.Models
{
    public class Note : Base
    {
        public string Text { get; set; }
        public ICollection<Image> Images { get; set; }
        
    }
}
